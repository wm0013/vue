const express = require('express');
const router = express.Router();
const usersController = require('../controller/UsersController')

/* GET users listing. */
router.get('/',usersController.getUser)
router.get('/:id',usersController.getUserID)
router.post('/add',usersController.addUser)
router.put('/update/',usersController.updateUser)
router.delete('/delete/:id',usersController.deleteUser)

module.exports = router
