const user = require('../models/user')
const userController = {
  async addUser(req, res) {
      const payload = req.body
      const users = new user(payload)
      try {
          await users.save()
          res.json(users)
      } catch (err) {
          res.status(500).send(err)
      }
  },
  async updateUser(req, res) {
      const payload = req.body
      try {
          const users = await user.updateOne({ _id: payload._id }, payload)
          res.json(users)
      } catch (err) {
          res.status(500).send(err)
      }
  },
  async deleteUser(req, res) {
      const { id } = req.params
      try {
          const users = await user.deleteOne({ _id: id })
          res.json(users)
      } catch (err) {
          res.status(500).send(err)
      }
  },
  async getUser(req, res) {
      try {
          const users = await user.find({})
          res.json(users)
      } catch (err) {
          res.status(500).send(err)
      }
  },
  async getUserID(req, res) {
      const { id } = req.params
      try {
          const users = await user.findById(id)
          res.json(users)
      } catch (err) {
          res.status(500).send(err)
      }
  }
}
module.exports = userController
